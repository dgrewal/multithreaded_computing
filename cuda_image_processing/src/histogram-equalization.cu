#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "hist-equ.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <device_functions.h>

__global__ void histogram(int* hist_out, unsigned char * img_in, int img_size, int nbr_bin)
{	int i = threadIdx.x + blockIdx.x * blockDim.x;
    if(i < img_size ) {atomicAdd(&hist_out[img_in[i]],1);}
/*
__shared__  unsigned int temp[256];
    temp[threadIdx.x] = 0;
    __syncthreads();
int i = threadIdx.x + blockIdx.x * blockDim.x;
if(i < size)
        atomicAdd( &temp[img_in[i]], 1 );
__syncthreads();
    atomicAdd( &(hist_out[threadIdx.x]), temp[threadIdx.x] );
*/
}
__global__ void calculate_lut(int* hist_in, int* lut_out, int img_size, int nbr_bin)
{
	int i, cdf, min, d;
    cdf = 0;
    min = 0;
    i = 0;
    if(min == 0){min = hist_in[i++];}
    d = img_size - min;
    for(i = 0; i < nbr_bin; i ++){
        cdf += hist_in[i];
        lut_out[i] = (int)(((float)cdf - min)*255/d + 0.5);
        if(lut_out[i] < 0){
            lut_out[i] = 0;
        }
	}    
}


__global__ void histogram_equalization(unsigned char * img_out, unsigned char * img_in,int* lut_in, int img_size, int nbr_bin){ 
int i = threadIdx.x + blockIdx.x * blockDim.x;
	if(i < img_size ){
		if(lut_in[img_in[i]] > 255){img_out[i] = 255;}
       else{img_out[i] = (unsigned char)lut_in[img_in[i]];}
    }
	
}
