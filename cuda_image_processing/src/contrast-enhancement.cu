#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "hist-equ.h"
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>

PGM_IMG contrast_enhancement_g(PGM_IMG img_in)
{
    PGM_IMG* result = (PGM_IMG*)malloc(img_in.w*img_in.h*sizeof(unsigned char)+2*sizeof(int));
    int* hist = (int*)malloc(256*sizeof(int));
    unsigned char* h_resimg = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	unsigned char* h_inimg = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	
	h_inimg=img_in.img;
	int *d_hist;
	unsigned char* d_resimg;
	unsigned char* d_inimg;
	int *lut = (int*)malloc(256*sizeof(int));
	int *d_lut;

	for(int i=0;i<256;i++){hist[i]=0;}

	 cudaError_t err_lut_mal=cudaMalloc((void**)&d_lut, 256*sizeof(int));
	if(cudaSuccess != err_lut_mal)
	{printf("Error allocating lut array: %s\n", cudaGetErrorString(err_lut_mal));}
	cudaError_t err_hist_mal=cudaMalloc((void**)&d_hist, 256*sizeof(int));
	if(cudaSuccess != err_hist_mal)
	{printf("Error allocating hist array to device: %s\n", cudaGetErrorString(err_hist_mal));}
	cudaError_t err_resimg_mal = cudaMalloc((void**)&d_resimg,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_resimg_mal)
	{printf("Error allocating resimg to device: %s\n", cudaGetErrorString(err_resimg_mal));}
	cudaError_t err_inimg_mal= cudaMalloc((void**)&d_inimg,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_inimg_mal)
	{printf("Error allocating inimg to device: %s\n", cudaGetErrorString(err_inimg_mal));}
	cudaError_t err_mcpy_lut = cudaMemcpy(d_lut,lut, 256*sizeof(int), cudaMemcpyHostToDevice);
	if(err_mcpy_lut!=cudaSuccess){printf("Error:  %s\n",cudaGetErrorString(err_mcpy_lut));}
	
	cudaMemcpy(d_hist,hist, 256*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_inimg,h_inimg,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	result->img = d_resimg;
	

	histogram<<<img_in.w*img_in.h/256,256>>>(d_hist, d_inimg, img_in.w*img_in.h, 256);
	calculate_lut<<<1,1>>>(d_hist,d_lut, img_in.w*img_in.h, 256);
	histogram_equalization<<<img_in.w*img_in.h/256,256>>>(d_resimg,d_inimg,d_lut,img_in.w * img_in.h, 256);

	//Memcpy back d_hist, d_img_in  and d_result in memory
	cudaMemcpy(h_resimg,d_resimg,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	cudaFree(d_hist);
	cudaFree(d_resimg);
	cudaFree(d_inimg);
	cudaFree(d_lut);
	result->h=img_in.h;
	result->w=img_in.w;
	result->img=h_resimg; 
    return *result;
}

PPM_IMG contrast_enhancement_c_rgb(PPM_IMG img_in)
{
    PPM_IMG* result = (PPM_IMG*)malloc(img_in.w*img_in.h*sizeof(unsigned char)+2*sizeof(int));
    int* hist = (int*)malloc(256*sizeof(int));
    
	unsigned char* h_inimg_r = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	h_inimg_r=img_in.img_r;
	unsigned char* h_inimg_g = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	h_inimg_g=img_in.img_g;
	unsigned char* h_inimg_b = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	h_inimg_b=img_in.img_b;

	unsigned char* h_resimg_r = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	unsigned char* h_resimg_g = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	unsigned char* h_resimg_b = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	
	int *d_hist;
	unsigned char* d_resimg_r; unsigned char*d_resimg_g; unsigned char*d_resimg_b;
	unsigned char* d_inimg_r ; unsigned char* d_inimg_g; unsigned char* d_inimg_b;

	int *lut = (int*)malloc(256*sizeof(int));
	int *d_lut;

	for(int i=0;i<256;i++){hist[i]=0;}

	 cudaError_t err_lut_mal=cudaMalloc((void**)&d_lut, 256*sizeof(int));
	if(cudaSuccess != err_lut_mal) {printf("Error allocating lut array: %s\n", cudaGetErrorString(err_lut_mal));}
	
	cudaError_t err_hist_mal=cudaMalloc((void**)&d_hist, 256*sizeof(int));
	if(cudaSuccess != err_hist_mal) {printf("Error allocating hist array to device: %s\n", cudaGetErrorString(err_hist_mal));}
	
	cudaError_t err_resimg_r_mal = cudaMalloc((void**)&d_resimg_r,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_resimg_r_mal) {printf("Error allocating resimg to device: %s\n", cudaGetErrorString(err_resimg_r_mal));}

	cudaError_t err_resimg_g_mal = cudaMalloc((void**)&d_resimg_g,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_resimg_g_mal) {printf("Error allocating resimg to device: %s\n", cudaGetErrorString(err_resimg_g_mal));}
	
	cudaError_t err_resimg_b_mal = cudaMalloc((void**)&d_resimg_b,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_resimg_b_mal) {printf("Error allocating resimg to device: %s\n", cudaGetErrorString(err_resimg_b_mal));}

	cudaError_t err_inimg_r_mal= cudaMalloc((void**)&d_inimg_r,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_inimg_r_mal) {printf("Error allocating inimg to device: %s\n", cudaGetErrorString(err_inimg_r_mal));}
	
	cudaError_t err_inimg_g_mal= cudaMalloc((void**)&d_inimg_g,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_inimg_g_mal) {printf("Error allocating inimg to device: %s\n", cudaGetErrorString(err_inimg_g_mal));}
	
	cudaError_t err_inimg_b_mal= cudaMalloc((void**)&d_inimg_b,img_in.w*img_in.h*sizeof(unsigned char));
	if(cudaSuccess != err_inimg_b_mal) {printf("Error allocating inimg to device: %s\n", cudaGetErrorString(err_inimg_b_mal));}
	

	cudaMemcpy(d_lut,lut, 256*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_hist,hist, 256*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_resimg_r,h_resimg_r,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(d_resimg_g,h_resimg_g,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(d_resimg_b,h_resimg_b,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);

	cudaMemcpy(d_inimg_r,h_inimg_r,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(d_inimg_g,h_inimg_g,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(d_inimg_b,h_inimg_b,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	
	result->img_r = d_resimg_r;
	result->img_g = d_resimg_g;
	result->img_b = d_resimg_b;

	histogram<<<img_in.w*img_in.h/256,256>>>(d_hist, d_inimg_r, img_in.w*img_in.h, 256);
	calculate_lut<<<1,1>>>(d_hist,d_lut,img_in.w*img_in.h, 256);
	histogram_equalization<<<img_in.w*img_in.h/256,256>>>(d_resimg_r,d_inimg_r,d_lut,img_in.w*img_in.h, 256);
	
	histogram<<<img_in.w*img_in.h/256,256>>>(d_hist, d_inimg_g, img_in.w*img_in.h, 256);
	calculate_lut<<<1,1>>>(d_hist,d_lut, img_in.w*img_in.h, 256);
	histogram_equalization<<<img_in.w*img_in.h/256,256>>>(d_resimg_g,d_inimg_g,d_lut,img_in.w*img_in.h, 256);
	
	histogram<<<img_in.w*img_in.h/256,256>>>(d_hist, d_inimg_b, img_in.w*img_in.h, 256);
	calculate_lut<<<1,1>>>(d_hist,d_lut, img_in.w*img_in.h, 256);
	histogram_equalization<<<img_in.w*img_in.h/256,256>>>(d_resimg_b,d_inimg_b,d_lut,img_in.w*img_in.h, 256);
    
    cudaMemcpy(hist, d_hist, 256*sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(lut, d_lut, 256*sizeof(int), cudaMemcpyDeviceToHost);
	cudaMemcpy(h_resimg_r,d_resimg_r,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	cudaMemcpy(h_resimg_g,d_resimg_g,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	cudaMemcpy(h_resimg_b,d_resimg_b,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	
	cudaMemcpy(h_inimg_r,d_inimg_r,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	cudaMemcpy(h_inimg_g,d_inimg_g,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	cudaMemcpy(h_inimg_b,d_inimg_b,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);

	//int x =0; for(int i=0;i<256;i++) { x= x+ hist[i];
	//printf("Hist:%d",hist[i]); printf("Sum%d\n",x);}
	cudaFree(d_hist);
	cudaFree(d_resimg_r);
	cudaFree(d_resimg_g);
	cudaFree(d_resimg_b);
	cudaFree(d_inimg_r);
	cudaFree(d_inimg_g);
	cudaFree(d_inimg_b);
	cudaFree(d_lut);
	result->h=img_in.h;
	result->w=img_in.w;
	result->img_r=h_resimg_r;
	result->img_g=h_resimg_g;
	result->img_b=h_resimg_b;
	return *result;
}

PPM_IMG contrast_enhancement_c_yuv(PPM_IMG img_in)
{
	YUV_IMG yuv_med;
    PPM_IMG result;
	unsigned char* h_y_equ;
	int *hist = (int*)malloc(256*sizeof(int));
	yuv_med = rgb2yuv(img_in);
	h_y_equ = (unsigned char *)malloc(yuv_med.h*yuv_med.w*sizeof(unsigned char));
	int *lut = (int*)malloc(256*sizeof(int));

	unsigned char* d_y_equ;
	int *d_hist;	
	int *d_lut;
	unsigned char* h_yuv_med_img_y = (unsigned char*)malloc(yuv_med.h*yuv_med.w*sizeof(unsigned char));
	h_yuv_med_img_y=yuv_med.img_y;
	unsigned char* d_yuv_med_img_y;
	for(int i=0;i<256;i++){hist[i]=0;}

	cudaMalloc((void**)&d_lut, 256*sizeof(int));
	cudaMalloc((void**)&d_hist, 256*sizeof(int));
	cudaError_t err_yuv_med_y = cudaMalloc((void**)&d_yuv_med_img_y, yuv_med.h*yuv_med.w*sizeof(unsigned char));
	if(err_yuv_med_y!=cudaSuccess){printf("error in malloc yuv_med_y%s\n",cudaGetErrorString(err_yuv_med_y));}
	cudaError_t err_d_y_equ = cudaMalloc((void**)&d_y_equ, yuv_med.h*yuv_med.w*sizeof(unsigned char));
	if(err_d_y_equ!=cudaSuccess){printf("error in malloc d_y_equ%s\n",cudaGetErrorString(err_d_y_equ));}
	
	cudaMemcpy(d_lut,lut, 256*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(d_hist,hist, 256*sizeof(int), cudaMemcpyHostToDevice);
	cudaError_t err1 = cudaMemcpy(d_yuv_med_img_y,h_yuv_med_img_y,yuv_med.h*yuv_med.w*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err1!=cudaSuccess){printf("error in memcpy d_yuv_med_img%s\n",cudaGetErrorString(err1));}
	cudaError_t err2 = cudaMemcpy(d_y_equ,h_y_equ,yuv_med.h*yuv_med.w*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err2!=cudaSuccess){printf("error in memcpy d_y_equ%s\n",cudaGetErrorString(err2));}
	
	histogram<<<img_in.w*img_in.h/256,256>>>(d_hist, d_yuv_med_img_y, yuv_med.h*yuv_med.w, 256);
	calculate_lut<<<1,1>>>(d_hist,d_lut, yuv_med.h*yuv_med.w, 256);
	histogram_equalization<<<img_in.w*img_in.h/256,256>>>(d_y_equ,d_yuv_med_img_y,d_lut,yuv_med.h*yuv_med.w, 256);

	cudaMemcpy(h_y_equ,d_y_equ,yuv_med.h*yuv_med.w*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	
	yuv_med.img_y = h_y_equ;
	result= yuv2rgb(yuv_med);

	free(yuv_med.img_y);
    free(yuv_med.img_u);
    free(yuv_med.img_v);
	cudaFree(d_hist);
	cudaFree(d_lut);
	cudaFree(d_yuv_med_img_y);
	cudaFree(d_y_equ);
	return result;
}

PPM_IMG contrast_enhancement_c_hsl(PPM_IMG img_in)
{
    HSL_IMG hsl_med;
    PPM_IMG result;
    
    unsigned char* h_l_equ;
    int* hist = (int*) malloc(256*sizeof(int));
    hsl_med = rgb2hsl(img_in);
    h_l_equ = (unsigned char*)malloc(hsl_med.height*hsl_med.width*sizeof(unsigned char));
	int* lut = (int*)malloc(256*sizeof(int));
	unsigned char* d_l_equ;
	int* d_hist;
	int* d_lut;
	unsigned char* h_hsl_med_img_l = (unsigned char*)malloc(img_in.w*img_in.h*sizeof(unsigned char));
	h_hsl_med_img_l = hsl_med.l;
	unsigned char* d_hsl_med_img_l;

	for(int i=0;i<256;i++){hist[i]=0;}
	
	cudaMalloc((void**)&d_lut,256*sizeof(int));
	cudaMalloc((void**)&d_hist,256*sizeof(int));
	cudaMalloc((void**)&d_hsl_med_img_l,img_in.w*img_in.h*sizeof(unsigned char));
	cudaMalloc((void**)&d_l_equ,img_in.w*img_in.h*sizeof(unsigned char));

	cudaMemcpy(d_hsl_med_img_l,h_hsl_med_img_l,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	cudaMemcpy(d_l_equ,h_l_equ,img_in.w*img_in.h*sizeof(unsigned char), cudaMemcpyHostToDevice);
	cudaMemcpy(d_lut,lut,256*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy(d_hist,hist,256*sizeof(int),cudaMemcpyHostToDevice);

	
	histogram<<<img_in.w*img_in.h/256,256>>>(d_hist, d_hsl_med_img_l,img_in.w*img_in.h, 256);
    calculate_lut<<<1,1>>>(d_hist,d_lut, img_in.w*img_in.h, 256);
	histogram_equalization<<<img_in.w*img_in.h/256,256>>>(d_l_equ, d_hsl_med_img_l,d_lut,img_in.w*img_in.h, 256);

	cudaMemcpy(h_l_equ,d_l_equ,img_in.w*img_in.h*sizeof(unsigned char), cudaMemcpyDeviceToHost);

	hsl_med.l= h_l_equ;
	result = hsl2rgb(hsl_med);
	cudaFree(d_hsl_med_img_l);
	cudaFree(d_l_equ);
	cudaFree(d_hist);
	cudaFree(d_lut);
    free(hsl_med.l);
    free(hsl_med.h);
    free(hsl_med.s);
    return result;
}



//Conversions
//from RGB to others
__global__ void convertrgb2hsl( float* H_out, float* S_out, unsigned char* L_out, unsigned char* r, unsigned char* g, unsigned char* b,int img_size)
{
	float H,S,L;
	int i = threadIdx.x + blockDim.x*blockIdx.x;
	if(i < img_size){
        H=0;S=0;L=0;
        float var_r = ( (float)r[i]/255 );//Convert RGB to [0,1]
        float var_g = ( (float)g[i]/255 );
        float var_b = ( (float)b[i]/255 );
        float var_min = (var_r < var_g) ? var_r : var_g;
        var_min = (var_min < var_b) ? var_min : var_b;   //min. value of RGB
        float var_max = (var_r > var_g) ? var_r : var_g;
        var_max = (var_max > var_b) ? var_max : var_b;   //max. value of RGB
        float del_max = var_max - var_min;               //Delta RGB value
        
        L = ( var_max + var_min ) / 2;
        if ( del_max == 0 )//This is a gray, no chroma...
        {
            H = 0;         
            S = 0;    
        }
        else                                    //Chromatic data...
        {
            if ( L < 0.5 )
                S = del_max/(var_max+var_min);
            else
                S = del_max/(2-var_max-var_min );

            float del_r = (((var_max-var_r)/6)+(del_max/2))/del_max;
            float del_g = (((var_max-var_g)/6)+(del_max/2))/del_max;
            float del_b = (((var_max-var_b)/6)+(del_max/2))/del_max;
            if( var_r == var_max ){
                H = del_b - del_g;
            }
            else{       
                if( var_g == var_max ){
                    H = (1.0/3.0) + del_r - del_b;
                }
                else{
                        H = (2.0/3.0) + del_g - del_r;
                }   
            }
            
        }
        
        if ( H < 0 )
            H += 1;
        if ( H > 1 )
            H -= 1;

        H_out[i] = H;
        S_out[i] = S;
        L_out[i] = (unsigned char)(L*255);
    }
}


HSL_IMG rgb2hsl(PPM_IMG img_in)
{
    HSL_IMG img_out;// = (HSL_IMG *)malloc(sizeof(HSL_IMG));
    img_out.width  = img_in.w;
    img_out.height = img_in.h;
    img_out.h = (float *)malloc(img_in.w * img_in.h * sizeof(float));
    img_out.s = (float *)malloc(img_in.w * img_in.h * sizeof(float));
    img_out.l = (unsigned char *)malloc(img_in.w * img_in.h * sizeof(unsigned char));
    
	float* d_out_h; 
	cudaError_t err_mal_outh = cudaMalloc((void**)&d_out_h,img_in.w*img_in.h*sizeof(float));
	if(err_mal_outh != cudaSuccess){printf("Error in malloc d_out_h: %s \n",cudaGetErrorString(err_mal_outh));}
	float* d_out_s; 
	cudaError_t err_mal_outs = cudaMalloc((void**)&d_out_s,img_in.w*img_in.h*sizeof(float));
	if(err_mal_outs!= cudaSuccess){printf("error malloc d_out_s:  %s\n",cudaGetErrorString(err_mal_outs));}
	unsigned char *d_out_l;
	cudaError_t err_mal_outl = cudaMalloc((void**)&d_out_l,img_in.w*img_in.h*sizeof(unsigned char));
	if(err_mal_outl != cudaSuccess){printf("Error in mcpy d_out_l: %s \n",cudaGetErrorString(err_mal_outl));}
	unsigned char *d_r;
	cudaError_t err_mal_r = cudaMalloc((void**)&d_r, img_in.h*img_in.w*sizeof(unsigned char));
	if(err_mal_r != cudaSuccess){printf("Error in malloc d_r: %s \n",cudaGetErrorString(err_mal_r));}
	cudaError_t err_mcpy_r = cudaMemcpy(d_r,img_in.img_r,img_in.h*img_in.w*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err_mcpy_r != cudaSuccess){printf("Error in mcopy d_r: %s \n",cudaGetErrorString(err_mcpy_r));}
	unsigned char *d_g;
	cudaError_t err_mal_g = cudaMalloc((void**)&d_g, img_in.h*img_in.w*sizeof(unsigned char));
	if(err_mal_g != cudaSuccess){printf("Error in malloc d_g: %s \n",cudaGetErrorString(err_mal_g));}
	cudaError_t err_mcpy_g = cudaMemcpy(d_g,img_in.img_g,img_in.h*img_in.w*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err_mcpy_g != cudaSuccess){printf("Error in mcpy d_g: %s \n",cudaGetErrorString(err_mcpy_g));}
	unsigned char *d_b; 
	cudaError_t err_mal_b = cudaMalloc((void**)&d_b, img_in.h*img_in.w*sizeof(unsigned char));
	if(err_mal_b!= cudaSuccess){printf("error malloc d_b: %s\n"),cudaGetErrorString(err_mal_b);}
	cudaError_t err_mcpy_b = cudaMemcpy(d_b,img_in.img_b,img_in.h*img_in.w*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err_mcpy_b != cudaSuccess){printf("Error in mcpy d_b: %s \n",cudaGetErrorString(err_mcpy_b));}

	convertrgb2hsl<<<img_in.h*img_in.w/256,256>>>(d_out_h,d_out_s,d_out_l,d_r,d_g,d_b,img_in.h*img_in.w);

	cudaError_t err_mcpy_outh = cudaMemcpy(img_out.h,d_out_h,img_in.w*img_in.h*sizeof(float),cudaMemcpyDeviceToHost);
	if(err_mcpy_outh!= cudaSuccess){printf("error mcpy back d_out_h: %s\n"),cudaGetErrorString(err_mcpy_outh);}
	cudaMemcpy(img_out.s,d_out_s,img_in.w*img_in.h*sizeof(float),cudaMemcpyDeviceToHost);
	cudaMemcpy(img_out.l,d_out_l,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
    
	cudaFree(d_out_h);
	cudaFree(d_out_s);
	cudaFree(d_out_l);
	cudaFree(d_r);
	cudaFree(d_g);
	cudaFree(d_b);
    return img_out;
}

__device__ float Hue_2_RGB( float v1, float v2, float vH )             //Function Hue_2_RGB
{
    if ( vH < 0 ) vH += 1;
    if ( vH > 1 ) vH -= 1;
    if ( ( 6 * vH ) < 1 ) return ( v1 + ( v2 - v1 ) * 6 * vH );
    if ( ( 2 * vH ) < 1 ) return ( v2 );
    if ( ( 3 * vH ) < 2 ) return ( v1 + ( v2 - v1 ) * ( ( 2.0f/3.0f ) - vH ) * 6 );
    return ( v1 );
}

//Convert HSL to RGB, assume H, S in [0.0, 1.0] and L in [0, 255]
//Output R,G,B in [0, 255]
__global__ void computehsl2rgb(float* imgH, float* imgS, unsigned char* imgL, unsigned char* imgr, unsigned char *imgg, unsigned char *imgb, int img_size)
{
	int i =threadIdx.x + blockIdx.x*blockDim.x;
	if(i < img_size){
        float H = imgH[i];
        float S = imgS[i];
        float L = imgL[i]/255.0f;
        float var_1, var_2;
        
        unsigned char r,g,b;
        
        if ( S == 0 )
        {
            r = L * 255;
            g = L * 255;
            b = L * 255;
        }
        else
        {
            
            if ( L < 0.5 )
                var_2 = L * ( 1 + S );
            else
                var_2 = ( L + S ) - ( S * L );

            var_1 = 2 * L - var_2;
            r = 255 * Hue_2_RGB( var_1, var_2, H + (1.0f/3.0f) );
            g = 255 * Hue_2_RGB( var_1, var_2, H );
            b = 255 * Hue_2_RGB( var_1, var_2, H - (1.0f/3.0f) );
        }
        imgr[i] = r;
        imgg[i] = g;
        imgb[i] = b;
    }
}

PPM_IMG hsl2rgb(HSL_IMG img_in)
{
    PPM_IMG result;
    
    result.w = img_in.width;
    result.h = img_in.height;
    result.img_r = (unsigned char *)malloc(result.w * result.h * sizeof(unsigned char));
    result.img_g = (unsigned char *)malloc(result.w * result.h * sizeof(unsigned char));
    result.img_b = (unsigned char *)malloc(result.w * result.h * sizeof(unsigned char));
    
	unsigned char* d_imgr, *d_imgg ,*d_imgb;
	cudaError_t err_mal_imgr = cudaMalloc((void**)&d_imgr,img_in.height*img_in.width*sizeof(unsigned char));
	if(err_mal_imgr){printf("error mal imgr:  %s\n",cudaGetErrorString(err_mal_imgr));}
	cudaError_t err_mal_imgg = cudaMalloc((void**)&d_imgg,img_in.height*img_in.width*sizeof(unsigned char));
	if(err_mal_imgg){printf("error mal imgg:  %s\n",cudaGetErrorString(err_mal_imgg));}
	cudaError_t err_mal_imgb = cudaMalloc((void**)&d_imgb,img_in.height*img_in.width*sizeof(unsigned char));
	if(err_mal_imgb){printf("error mal imgb:  %s\n",cudaGetErrorString(err_mal_imgb));}

	float* d_imgh,*d_imgs; unsigned char*d_imgl;
	cudaError_t err_mal_imgh = cudaMalloc((void**)&d_imgh,img_in.height*img_in.width*sizeof(float));
    if(err_mal_imgh){printf("error mal imgh:  %s\n",cudaGetErrorString(err_mal_imgh));}
	cudaError_t err_mal_imgs = cudaMalloc((void**)&d_imgs,img_in.height*img_in.width*sizeof(float));
	if(err_mal_imgs){printf("error mal imgs:  %s\n",cudaGetErrorString(err_mal_imgs));}
	cudaError_t err_mal_imgl = cudaMalloc((void**)&d_imgl,img_in.height*img_in.width*sizeof(unsigned char));
	if(err_mal_imgl){printf("error mal imgl:  %s\n",cudaGetErrorString(err_mal_imgl));}

	cudaError_t err_mcpy_imgh = cudaMemcpy(d_imgh,img_in.h,img_in.height*img_in.width*sizeof(float),cudaMemcpyHostToDevice);
	if(err_mcpy_imgh){printf("error mcpy imgh:  %s\n",cudaGetErrorString(err_mcpy_imgh));}
	cudaError_t err_mcpy_imgs = cudaMemcpy(d_imgs,img_in.s,img_in.height*img_in.width*sizeof(float),cudaMemcpyHostToDevice);
	if(err_mcpy_imgs){printf("error mcpy imgs:  %s\n",cudaGetErrorString(err_mcpy_imgs));}
	cudaError_t err_mcpy_imgl = cudaMemcpy(d_imgl,img_in.l,img_in.height*img_in.width*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err_mcpy_imgl){printf("error mcpy imgl:  %s\n",cudaGetErrorString(err_mcpy_imgl));}

	computehsl2rgb<<<img_in.height*img_in.width/256,256>>>(d_imgh,d_imgs,d_imgl,d_imgr,d_imgg,d_imgb,img_in.height*img_in.width);

	
	cudaError_t err_mcpy_imgr = cudaMemcpy(result.img_r,d_imgr,img_in.height*img_in.width*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	if(err_mcpy_imgr){printf("error mcpy imgr:  %s\n",cudaGetErrorString(err_mcpy_imgr));}
	cudaError_t err_mcpy_imgg= cudaMemcpy(result.img_g,d_imgg,img_in.height*img_in.width*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	if(err_mcpy_imgg){printf("error mcpy imgg:  %s\n",cudaGetErrorString(err_mcpy_imgg));}
	cudaError_t err_mcpy_imgb = cudaMemcpy(result.img_b,d_imgb,img_in.height*img_in.width*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	if(err_mcpy_imgb){printf("error mcpy imgb:  %s\n",cudaGetErrorString(err_mcpy_imgb));}
	cudaFree(d_imgr);
	cudaFree(d_imgg);
	cudaFree(d_imgb);
	cudaFree(d_imgh);
	cudaFree(d_imgs);
	cudaFree(d_imgl);
    return result;
}


__global__ void computergb2yuv(unsigned char*img_r,unsigned char*img_g,unsigned char*img_b,unsigned char*img_y,unsigned char*img_u,unsigned char*img_v,int img_size)
{
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	unsigned char y, cb, cr;
	unsigned char r, g, b;
	if(i < img_size){
		y=0;cb=0;cr=0;r=0;g=0;b=0;
        r = img_r[i];
        g = img_g[i];
        b = img_b[i];
        
        y  = (unsigned char)( 0.299*(r) + 0.587*(g) +  0.114*(b));
        cb = (unsigned char)(-0.169*(r) - 0.331*(g) +  0.499*(b )+ 128);
        cr = (unsigned char)( 0.499*(r) - 0.418*(g) - 0.0813*(b )+ 128);
        
        img_y[i] = y;
        img_u[i] = cb;
        img_v[i] = cr;
    }
    


}

//Convert RGB to YUV, all components in [0, 255]
YUV_IMG rgb2yuv(PPM_IMG img_in)
{
    YUV_IMG img_out;
    img_out.w = img_in.w;
    img_out.h = img_in.h;
	img_out.img_y = (unsigned char*)malloc(img_out.h*img_out.w*sizeof(unsigned char));
	img_out.img_u = (unsigned char*)malloc(img_out.h*img_out.w*sizeof(unsigned char));
	img_out.img_v = (unsigned char*)malloc(img_out.h*img_out.w*sizeof(unsigned char));
    

	unsigned char *d_img_r, *d_img_g, *d_img_b;
	cudaError_t err_mal_imgr = cudaMalloc((void**)&d_img_r, img_in.w*img_in.h*sizeof(unsigned char));
	if(err_mal_imgr){printf("error mal imgr:  %s\n",cudaGetErrorString(err_mal_imgr));}
	cudaError_t err_mcpy_imgr = cudaMemcpy(d_img_r,img_in.img_r,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err_mcpy_imgr){printf("error mcpy imgr:  %s\n",cudaGetErrorString(err_mcpy_imgr));}
	cudaError_t err_mal_imgg = cudaMalloc((void**)&d_img_g, img_in.w*img_in.h*sizeof(unsigned char));
	if(err_mal_imgg){printf("error mal imgg:  %s\n",cudaGetErrorString(err_mal_imgg));}
	cudaError_t err_mcpy_imgg = cudaMemcpy(d_img_g,img_in.img_g,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err_mcpy_imgg){printf("error mcpy imgg:  %s\n",cudaGetErrorString(err_mcpy_imgg));}
	cudaError_t err_mal_imgb = cudaMalloc((void**)&d_img_b, img_in.w*img_in.h*sizeof(unsigned char));
	if(err_mal_imgb){printf("error mal imgb:  %s\n",cudaGetErrorString(err_mal_imgb));}
	cudaError_t err_mcpy_imgb = cudaMemcpy(d_img_b,img_in.img_b,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyHostToDevice);
	if(err_mcpy_imgb){printf("error mcpy imgb:  %s\n",cudaGetErrorString(err_mcpy_imgb));}
	

	unsigned char *d_img_y, *d_img_u, *d_img_v;
	cudaError_t err_mal_imgy = cudaMalloc((void**)&d_img_y, img_in.w*img_in.h*sizeof(unsigned char));
	if(err_mal_imgy){printf("error mal imgy:  %s\n",cudaGetErrorString(err_mal_imgy));}
	cudaError_t err_mal_imgu = cudaMalloc((void**)&d_img_u, img_in.w*img_in.h*sizeof(unsigned char));
	if(err_mal_imgu){printf("error mal imgu:  %s\n",cudaGetErrorString(err_mal_imgu));}
	cudaError_t err_mal_imgv = cudaMalloc((void**)&d_img_v, img_in.w*img_in.h*sizeof(unsigned char));
	if(err_mal_imgv){printf("error mal imgv:  %s\n",cudaGetErrorString(err_mal_imgv));}

	computergb2yuv<<<img_in.h*img_in.w/256,256>>>(d_img_r,d_img_g,d_img_b,d_img_y,d_img_u,d_img_v,img_in.w*img_in.h);

	cudaError_t err_mcpy_imgy = cudaMemcpy(img_out.img_y,d_img_y,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	if(err_mcpy_imgy){printf("error mcpy imgy:  %s\n",cudaGetErrorString(err_mcpy_imgy));}
	cudaError_t err_mcpy_imgu = cudaMemcpy(img_out.img_u,d_img_u,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	if(err_mcpy_imgu){printf("error mcpy imgu:  %s\n",cudaGetErrorString(err_mcpy_imgu));}
	cudaError_t err_mcpy_imgv = cudaMemcpy(img_out.img_v,d_img_v,img_in.w*img_in.h*sizeof(unsigned char),cudaMemcpyDeviceToHost);
	if(err_mcpy_imgv){printf("error mcpy imgv:  %s\n",cudaGetErrorString(err_mcpy_imgv));}

	cudaFree(d_img_r);
	cudaFree(d_img_g);
	cudaFree(d_img_b);
	cudaFree(d_img_y);
	cudaFree(d_img_u);
	cudaFree(d_img_v);
    
    return img_out;
}

__device__ unsigned char clip_rgb(int x)
{
    if(x > 255)
        return 255;
    if(x < 0)
        return 0;

    return (unsigned char)x;
}

__global__ void computeyuv2rgb(unsigned char* img_y, unsigned char* img_u,unsigned char* img_v,unsigned char* img_r, unsigned char* img_g,unsigned char* img_b,int img_size)
{int y, cb, cr;
int  rt,gt,bt;
int i = threadIdx.x+blockIdx.x*blockDim.x;

	if(i < img_size){
		y=0;cb=0;cr=0;rt=0;gt=0;bt=0;
        y  = (int)img_y[i];
        cb = (int)img_u[i] - 128;
        cr = (int)img_v[i] - 128;
        
        rt  = (int)( y + 1.402*cr);
        gt  = (int)( y - 0.344*cb - 0.714*cr);
        bt  = (int)( y + 1.772*cb);

        img_r[i] = clip_rgb(rt);
        img_g[i] = clip_rgb(gt);
        img_b[i] = clip_rgb(bt);
    }

}

//Convert YUV to RGB, all components in [0, 255]
PPM_IMG yuv2rgb(YUV_IMG img_in)
{
    PPM_IMG img_out;
    img_out.w = img_in.w;
    img_out.h = img_in.h;
	img_out.img_r = (unsigned char*)malloc(img_in.h*img_in.w*sizeof(unsigned char));
	img_out.img_g = (unsigned char*)malloc(img_in.h*img_in.w*sizeof(unsigned char));
	img_out.img_b = (unsigned char*)malloc(img_in.h*img_in.w*sizeof(unsigned char));

	unsigned char *d_img_r, *d_img_g, *d_img_b;
    cudaMalloc((void**)&d_img_r,sizeof(unsigned char)*img_in.w*img_in.h);
    cudaMalloc((void**)&d_img_g,sizeof(unsigned char)*img_in.w*img_in.h);
    cudaMalloc((void**)&d_img_b,sizeof(unsigned char)*img_in.w*img_in.h);
	unsigned char * d_img_y,*d_img_u, *d_img_v;
	cudaMalloc((void**)&d_img_y,sizeof(unsigned char)*img_in.w*img_in.h);
	cudaMalloc((void**)&d_img_u,sizeof(unsigned char)*img_in.w*img_in.h);
	cudaMalloc((void**)&d_img_v,sizeof(unsigned char)*img_in.w*img_in.h);

	cudaMemcpy(d_img_y,img_in.img_y,sizeof(unsigned char)*img_in.w*img_in.h,cudaMemcpyHostToDevice);
	cudaError_t erru = cudaMemcpy(d_img_u,img_in.img_u,sizeof(unsigned char)*img_in.w*img_in.h,cudaMemcpyHostToDevice);
	if(erru!=cudaSuccess){printf("Error:  %s\n",cudaGetErrorString(erru));}
	cudaError_t err = cudaMemcpy(d_img_v,img_in.img_v,sizeof(unsigned char)*img_in.w*img_in.h,cudaMemcpyHostToDevice);
	if(err!=cudaSuccess){printf("Error:  %s\n",cudaGetErrorString(err));}
    
	computeyuv2rgb<<<img_in.w*img_in.h/256,256>>>(d_img_y,d_img_u,d_img_v,d_img_r,d_img_g,d_img_b,img_in.w*img_in.h);
    
	cudaMemcpy(img_out.img_r,d_img_r,sizeof(unsigned char)*img_in.w*img_in.h,cudaMemcpyDeviceToHost);
	cudaMemcpy(img_out.img_g,d_img_g,sizeof(unsigned char)*img_in.w*img_in.h,cudaMemcpyDeviceToHost);
    cudaMemcpy(img_out.img_b,d_img_b,sizeof(unsigned char)*img_in.w*img_in.h,cudaMemcpyDeviceToHost);
	
	cudaFree(d_img_r);
	cudaFree(d_img_g);
	cudaFree(d_img_b);
	cudaFree(d_img_y);
	cudaFree(d_img_u);
	cudaFree(d_img_v);

	return img_out;
}
