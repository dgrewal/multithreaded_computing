#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <cstdlib>
#include <string.h>
#include <assert.h>
#include "atomic_ops.h"

inline double getMHZ_x86(void)
{
    double mhz = -1;
    char line[1024], *s, search_str[] = "cpu MHz";
    FILE* fp;

    // open proc/cpuinfo
    if ((fp = fopen("/proc/cpuinfo", "r")) == NULL)
        return -1;

    // ignore all lines until we reach MHz information
    while (fgets(line, 1024, fp) != NULL) {
        if (strstr(line, search_str) != NULL) {
            // ignore all characters in line up to :
            for (s = line; *s && (*s != ':'); ++s);
            // get MHz number
            if (*s && (sscanf(s+1, "%lf", &mhz) == 1))
                break;
        }
    }

    if (fp != NULL)
        fclose(fp);
    return mhz;
}
inline unsigned long long gethrcycle_x86()
{
    unsigned int tmp[2];

    asm ("rdtsc"
         : "=a" (tmp[1]), "=d" (tmp[0])
         : "c" (0x10) );
    return (((unsigned long long)tmp[0] << 32 | tmp[1]));
}

// get the elapsed time (in nanoseconds) since startup
inline unsigned long long getElapsedTime()
{
    static double CPU_MHZ = 0;
    if (CPU_MHZ == 0)
        CPU_MHZ = getMHZ_x86();
    return (unsigned long long)(gethrcycle_x86() * 1000 / CPU_MHZ);
}



int i, insno,delno, t;

struct node_t;

struct pointer_t
{
	struct node_t *ptr;
	unsigned int Count;
};

struct node_t
{
   int value;
   struct pointer_t next;
};

struct Queue_t
{
   struct pointer_t Head, Tail;
};


class threadqueue
{
    public:
    node_t* heads;
    node_t* tails;
    threadqueue()
    {
        heads=tails=NULL;
    }
    node_t* releasenode();
    void receivenode();
    void show();
    void populate();
};

void threadqueue::populate()
{
    for(int j=0;j<i;j++)
    {
    struct node_t* temp= new node_t;
    temp->value=j;
    temp->next.ptr=NULL;
    if(heads==NULL)
    {
        heads=temp;
    }
    else
    {
        tails->next.ptr=temp;
    }
    tails=temp;
    }
}



node_t* threadqueue:: releasenode()
{
    if(heads==tails)
    {
        return NULL;
    }
    node_t* nodetorel=heads;
    heads=heads->next.ptr;
    nodetorel->value=0;
    nodetorel->next.ptr=NULL;
    nodetorel->next.Count=0;
    return nodetorel ;
}

void threadqueue::show()
{
   struct node_t *ptr1=heads;
   int items=0;
   while(ptr1!=NULL)
   {
       items++;
       ptr1=ptr1->next.ptr;
   }
   printf("\nNo. of items in thread queue %i",items);
}

threadqueue* tq= new threadqueue();


Queue_t *Q=NULL;

void init(Queue_t* Q)
{
node_t* node = new node_t;
node->next.ptr = NULL;
Q->Head.ptr = node;
Q->Tail.ptr = node;
}




void enqueue(Queue_t *Q,int value)
{
   // printf("\ninside enqueue");
   struct node_t *node = tq->releasenode();
   node->value=value;
   node->next.ptr=NULL;
    pointer_t* tail;
    pointer_t* next;
while(1)  //until insert is done//
{

tail=&Q->Tail;
next=&tail->ptr->next;

if(tail==&Q->Tail)
  {
       if(next->ptr==NULL)
       {
           if(casX((volatile unsigned long long*)&tail->ptr->next,next->Count,*((unsigned long*)&(next->ptr)),next->Count+1,*((unsigned long*)&(node))))
           {
               break;
           }
       }
       else
        {
         casX( (volatile unsigned long long*) &Q->Tail,  tail->Count,  *((unsigned long*)&(tail->ptr)), tail->Count+1, * ( (unsigned long*) &(next->ptr)) );
        }
   }
}
casX( (volatile unsigned long long*) &Q->Tail, tail->Count, *((unsigned long*)&(tail->ptr)), tail->Count+1,  * ( (unsigned long*)& (node)) );

//printf("\nEnqueue Done");
return;
}


bool dequeue(Queue_t *Q, int *pvalue)
{
    //printf("\ninside Dequeue");
    struct pointer_t* head=NULL;
    struct pointer_t* tail=NULL;
    struct pointer_t* next=NULL;
    while(1)
    {

        head=&Q->Head;
        tail=&Q->Tail;
        next=&head->ptr->next;

        if(head==&Q->Head)
        {
            if (head->ptr == tail->ptr)
            {
                if(next->ptr == NULL)
                        return false;
              casX( (volatile unsigned long long*) &Q->Tail, tail->Count, *((unsigned long*)&(tail->ptr)), tail->Count+1,  * ( (unsigned long*)& (next->ptr)) );
            }
            else
                *pvalue=next->ptr->value;
            if(casX( (volatile unsigned long long*) &Q->Head,  head->Count,  *((unsigned long*)&(head->ptr)), head->Count+1, * ( (unsigned long*) &(next->ptr)) ))
                    break;

            }
        }
   // printf("\nDequeue Done");
  free (head->ptr);
   return true;
}


void show()
{
   pointer_t *ptr1=&Q->Head;
   if(&Q->Head==NULL)
   {
      printf("\n The Queue is empty!!");
      return;
   }
   printf("\nThe Queue is\n");
   printf("START----");
   while(ptr1!=NULL)
   {
      printf("-%d",ptr1->ptr->value);
      ptr1=&(ptr1->ptr->next);
   }
   printf("----END\n");
}




int n;
pthread_barrier_t barr;
void *thread_function(void *arg)
{
    tq->populate();

    int rc = pthread_barrier_wait(&barr);
    if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
    {
        printf("Could not wait on barrier\n");
        exit(-1);
   }
  int pvalue=0;
    bool random[100000];
     for(int k=0;k<100000;k++)
        {
        random[k]=rand()%2;
        }
	int k;
    for(k=0;k<i;k++)
    {
        if(random[k]==0)
			  {
				  dequeue(Q,&pvalue);
			  }
			  else
			  {
				  enqueue(Q,k);
			  }
    }
    pthread_barrier_wait(&barr);
    return NULL;

}

int main (int argc, char *argv[])
{
    unsigned long long t1=getElapsedTime();
    Q = new Queue_t;
	init(Q);
	srand((unsigned)time(0));
	if(argc==1)
    {
       printf("Running Default 4 threads and 1000 iterations\n");
                t=4;
                i=1000;
    }
   else if (argc!=5)
    {
        printf("usage is -t threads -i iterations\n");
    }
    else
    {
        for(int x=1;x<argc-1;x++)
        {
	  if(x+1<argc)
	  {
              if(strcmp(argv[x], "-t")==0)
              {
                t=atoi(argv[x+1]);
              }
                else if(strcmp(argv[x], "-i")==0)
              {
                i=atoi(argv[x+1]);
              }
 	   }
            else
            {
                printf("\n invalid argument number %i",argc);
            }

       }
    }


	pthread_t mythread[t];

	if(pthread_barrier_init(&barr, NULL, t))
    {
        printf("Could not create a barrier\n");
        return -1;
    }
	for ( n=0; n<t; n++)
		{
		  if (pthread_create(&mythread[n], NULL, thread_function, NULL))
			{
			 printf("Error Creating Thread");
			 abort();
			}
		}
	for ( n=0; n<t; n++)
	{
		if(pthread_join( mythread[n], NULL))
		{
		  printf("Error Joining Thread");
		  abort();
		}
	}
    unsigned long long t2=getElapsedTime();
	printf ("\n Time Elapsed : %llu\n", t2-t1);
	tq->show();
	//show();
	exit(0);

}
