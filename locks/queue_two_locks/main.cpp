#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <cstdlib>
#include <string.h>
#include <assert.h>


inline double getMHZ_x86(void)
{
    double mhz = -1;
    char line[1024], *s, search_str[] = "cpu MHz";
    FILE* fp;

    // open proc/cpuinfo
    if ((fp = fopen("/proc/cpuinfo", "r")) == NULL)
        return -1;

    // ignore all lines until we reach MHz information
    while (fgets(line, 1024, fp) != NULL) {
        if (strstr(line, search_str) != NULL) {
            // ignore all characters in line up to :
            for (s = line; *s && (*s != ':'); ++s);
            // get MHz number
            if (*s && (sscanf(s+1, "%lf", &mhz) == 1))
                break;
        }
    }

    if (fp != NULL)
        fclose(fp);
    return mhz;
}
inline unsigned long long gethrcycle_x86()
{
    unsigned int tmp[2];

    asm ("rdtsc"
         : "=a" (tmp[1]), "=d" (tmp[0])
         : "c" (0x10) );
    return (((unsigned long long)tmp[0] << 32 | tmp[1]));
}

// get the elapsed time (in nanoseconds) since startup
inline unsigned long long getElapsedTime()
{
    static double CPU_MHZ = 0;
    if (CPU_MHZ == 0)
        CPU_MHZ = getMHZ_x86();
    return (unsigned long long)(gethrcycle_x86() * 1000 / CPU_MHZ);
}


unsigned long long t1=getElapsedTime();

//   Creating a NODE Structure
struct node
{
   int data;
   struct node *next;
};

class locking
{
    public:
    volatile int lval;
    bool lck;
    void lock();
    void unlock();
};
    void locking::lock()
    {
        while (lck == 1)
        {

            while (__sync_lock_test_and_set(&lval, 1))
                {
                    //int delay=1;
                    //sleep(delay);
                    //delay=delay * 2;
                }
			lval=(bool)lck;
        }
    }
    void locking::unlock()
    {
     __sync_lock_release(&lval);
     lval=(bool)lck;
    }



int i, insno,delno, t;
class threadqueue
{
    public:
    node* heads;
    node* tails;
    threadqueue()
    {
        heads=tails=NULL;
    }
    node* releasenode();
    void receivenode();
    void show();
    void populate();
};

void threadqueue::populate()
{
    for(int j=0;j<i;j++)
    {
    struct node* temp= new node;
    temp->data=j;
    temp->next=NULL;
    if(heads==NULL)
    {
        heads=temp;
    }
    else
    {
        tails->next=temp;
    }
    tails=temp;
    }
}



node* threadqueue:: releasenode()
{
    if(heads==tails)
    {
        return NULL;
    }
    node* nodetorel=heads;
    heads=heads->next;
    nodetorel->data=0;
    nodetorel->next=NULL;
    return nodetorel ;
}

void threadqueue::show()
{
   struct node *ptr1=heads;
   int items=0;
   while(ptr1!=NULL)
   {
       items++;
       ptr1=ptr1->next;
   }
   printf("\nNo. of items in thread queue %i",items);
}

threadqueue* tq= new threadqueue();

class queue : public threadqueue, public locking
{
   struct node *frnt,*rear;
   public:
      queue() // constructor
      {
	 frnt=rear=NULL;
      }
      void insert(int value); // to insert an element
      void del();  // to delete an element
      void show(); // to show the stack
};
// Insertion
void queue::insert(int value)
{
   node *ptr=tq->releasenode();
   if(ptr==NULL)
   return;

   ptr->data=value;
   ptr->next=NULL;
    insno++;
    lock();
   if(frnt==NULL)
      {
          frnt=ptr;
          rear=ptr;
      }
  else
     {
    rear->next=ptr;
   rear=ptr;
     }
    unlock();
}

// Deletion
void queue::del()
{
    lock();
   if(frnt==NULL)
   {
      return;
   }
   struct node *temp;
   temp=frnt;
   frnt=frnt->next;
   delete temp;
   unlock();
}

// Show Queue
void queue::show()
{
   struct node *ptr1=frnt;
   if(frnt==NULL)
   {
      printf("\n The Queue is empty!!");
      return;
   }
   printf("\nThe Queue is\n");
   printf("START----");
   while(ptr1!=NULL)
   {
      printf("-%d",ptr1->data);
      ptr1=ptr1->next;
   }
   printf("----END\n");
}

volatile int lval;
void acquire_lock()
{
        int delay=6;
		while (__sync_lock_test_and_set(&lval, 1))
		{
		sleep(delay);
		delay=delay*2;
		}
}
void release_lock()
{
	__sync_lock_release(&lval);
}


queue q1;
pthread_barrier_t barr;
void *thread_function(void *arg)
{
    tq->populate();

    int rc = pthread_barrier_wait(&barr);
    if(rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
    {
        printf("Could not wait on barrier\n");
        exit(-1);
    }
    bool random[100000];
     for(int k=0;k<100000;k++)
        {
        random[k]=rand()%2;
        }
	int k;
				// i is the No. of Iterations j,k are Temporary Variables
		for (k=0; k<i; k++)
			{
			    acquire_lock();
			  if(random[k]==0)
			  {
				  q1.del();
			  }
			  else
			  {
				  q1.insert(k);
			  }
			  release_lock();
			}
			pthread_barrier_wait(&barr);

            return NULL;
}

int main (int argc, char *argv[])
{
	int t, n;						// No. of Threads
	srand((unsigned)time(0));
	if(argc==1)
    {
       printf("Running Default 4 threads and 10000 iterations\n");
                t=4;
                i=10000;
    }
   else if (argc!=5)
    {
        printf("usage is -t threads -i iterations\n");
    }
    else
    {
        for(int x=1;x<argc-1;x++)
        {
	  if(x+1<argc)
	  {
              if(strcmp(argv[x], "-t")==0)
              {
                t=atoi(argv[x+1]);
              }
                else if(strcmp(argv[x], "-i")==0)
              {
                i=atoi(argv[x+1]);
              }
 	   }
            else
            {
                printf("\n invalid argument number %i",argc);
            }

       }
    }

	pthread_t mythread[t];
if(pthread_barrier_init(&barr, NULL, t))
    {
        printf("Could not create a barrier\n");
        return -1;
    }
	for ( n=0; n<t; n++)
		{
		  if (pthread_create(&mythread[n], NULL, thread_function, NULL))
			{
			 printf("Error Creating Thread");
			 abort();
			}
		}
	for ( n=0; n<t; n++)
	{
		if(pthread_join( mythread[n], NULL))
		{
		  printf("Error Joining Thread");
		  abort();
		}
	}
    unsigned long long t2=getElapsedTime();
	printf (" \n\nTime Elapsed : %llu\n",(t2-t1));
    q1.show();
    tq->show();
	exit(0);
}
