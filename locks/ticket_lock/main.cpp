#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <cstdlib>
#include <string.h>
#include <assert.h>
#include "atomic_ops.h"

inline double getMHZ_x86(void)
{
    double mhz = -1;
    char line[1024], *s, search_str[] = "cpu MHz";
    FILE* fp;

    // open proc/cpuinfo
    if ((fp = fopen("/proc/cpuinfo", "r")) == NULL)
        return -1;

    // ignore all lines until we reach MHz information
    while (fgets(line, 1024, fp) != NULL) {
        if (strstr(line, search_str) != NULL) {
            // ignore all characters in line up to :
            for (s = line; *s && (*s != ':'); ++s);
            // get MHz number
            if (*s && (sscanf(s+1, "%lf", &mhz) == 1))
                break;
        }
    }

    if (fp != NULL)
        fclose(fp);
    return mhz;
}
inline unsigned long long gethrcycle_x86()
{
    unsigned int tmp[2];

    asm ("rdtsc"
         : "=a" (tmp[1]), "=d" (tmp[0])
         : "c" (0x10) );
    return (((unsigned long long)tmp[0] << 32 | tmp[1]));
}

// get the elapsed time (in nanoseconds) since startup
inline unsigned long long getElapsedTime()
{
    static double CPU_MHZ = 0;
    if (CPU_MHZ == 0)
        CPU_MHZ = getMHZ_x86();
    return (unsigned long long)(gethrcycle_x86() * 1000 / CPU_MHZ);
}


int counter, i;  						// Global Variable for Counter

        volatile int next_ticket;
        volatile int now_serving;

    static inline void acquire_lock()
    {
        volatile int my_ticket = __sync_fetch_and_add(&next_ticket,1);
        while (now_serving != my_ticket)
        {
        	sleep(my_ticket-now_serving);
        }
    }

    static inline void release_lock()
    {
        now_serving += 1;
    }

ticket_lock_t* L=new ticket_lock_t;
void *thread_function(void *arg)
{
	int j, k; 					// i is the No. of Iterations j,k are Temporary Variables
	for (k=0; k<i; k++)
		{
		ticket_acquire(L);
		  j = counter;				// Increment Counter i times
		  j = j+1;
		  counter = j;
		 ticket_release(L);
		}
		return NULL;
}

int main (int argc, char *argv[])
{
int t, n;						// No. of Threads;
    if(argc==1)
    {
       printf("Running Default 4 threads and 10000 iterations\n");
                t=4;
                i=10000;
    }
   else if (argc!=5)
    {
        printf("usage is -t threads -i iterations\n");
    }
    else
    {
        for(int x=1;x<argc-1;x++)
        {
	  if(x+1<argc)
	  {
              if(strcmp(argv[x], "-t")==0)
              {
                t=atoi(argv[x+1]);
              }
                else if(strcmp(argv[x], "-i")==0)
              {
                i=atoi(argv[x+1]);
              }
 	   }
            else
            {
                printf("\n invalid argument number %i",argc);
            }

       }
    }


	pthread_t mythread[t];
	for ( n=0; n<t; n++)
		{
		  if (pthread_create(&mythread[n], NULL, thread_function, NULL))
			{
			 printf("Error Creating Thread");
			 abort();
			}
		}
	for ( n=0; n<t; n++)
	{
		if(pthread_join( mythread[n], NULL))
		{
		  printf("Error Joining Thread");
		  abort();
		}
	}

	printf("Counter Value is %d \n", counter);
	printf (" Time Elapsed : %llu\n", getElapsedTime());
	exit(0);
}

