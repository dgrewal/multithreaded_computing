// Penn Parallel Primitives Library
// Author: Prof. Milo Martin
// University of Pennsylvania
// Spring 2010

#ifndef PPP_FOR_H
#define PPP_FOR_H

#include "ppp.h"
#include "Task.h"
#include "TaskGroup.h"

namespace ppp {
   namespace internal {
  
    template <typename T>
    class ForTask: public ppp::Task {
    public:
      ForTask(int64_t start, int64_t end, int64_t grainsize, T* functor) { 
        m_start = start;
        m_end = end;
        m_grainsize = grainsize;
	m_functor = functor;
      }

      void execute()
      {
       PPP_DEBUG_MSG("Execute: [" + to_string(m_start) + ", " + to_string(m_end) + "]");
       assert(m_start < m_end);

        if (m_end-m_start <= 1) {
          return;
        }
        
        if (m_end-m_start < m_grainsize) {       
         m_functor->calculate(m_start,m_end); 
          return;
        }
        
        int64_t mid = (m_start+m_end)/2;
        assert(mid < m_end);
        
        PPP_DEBUG_MSG("Split: [" + to_string(m_start) + ", " + to_string(mid) + "] [" +
                      to_string(mid) + ", " + to_string(m_end) + "]");
        ppp::TaskGroup tg;
        ForTask t1( m_start, mid, m_grainsize, m_functor);
        ForTask t2( mid, m_end, m_grainsize, m_functor);
        tg.spawn(t1);
        tg.spawn(t2);
        tg.wait();
      }
    private:
      int64_t m_start;
      int64_t m_end;
      int64_t m_grainsize;
      T* m_functor;
    };
  }
    
  template <typename T>
  extern inline
  void parallel_for(int64_t start, int64_t end, T* functor, int64_t grainsize=0)
  {
     if (grainsize == 0) {
      grainsize = (end-start+1) / (get_thread_count()*4);
    }
    PPP_DEBUG_MSG("parallel_for grainsize: " + to_string(grainsize));
            
    internal::ForTask<T> t(start, end, grainsize,functor); 
    t.execute();
    PPP_DEBUG_MSG("parallel_for done");
 }
}

#endif

