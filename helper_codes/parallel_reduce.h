// Penn Parallel Primitives Library
// Author: Prof. Milo Martin
// University of Pennsylvania
// Spring 2010

#ifndef PPP_REDUCE_H
#define PPP_REDUCE_H

#include "ppp.h"
#include "Task.h"
#include "TaskGroup.h"

namespace ppp {
namespace internal {
     template <typename T>
     extern inline
     int64_t sum(T* array, int64_t start, int64_t end)
     {
      T sum;
     sum = T(0);
     for (int i=start; i<end; i++) {
       sum = sum + array[i];
     }
     return sum;
     }

 template <typename T>
     class ReduceTask: public ppp::Task {
     public:
       ReduceTask(T* array,int64_t start, int64_t end, int64_t grainsize) {
         m_array=array;
	 m_start = start;
         m_end = end;
         m_grainsize = grainsize;
       }
 
       void execute()
       {
        PPP_DEBUG_MSG("Execute: [" + to_string(m_start) + ", " + to_string(m_end) + "]");
        assert(m_start < m_end);
 
         if (m_end-m_start <= 1) {
           return;
         }
 
         if (m_end-m_start < m_grainsize) {  
           internal::sum(m_array,m_start,m_end);
	 return;
         }
 
         int64_t mid = (m_start+m_end)/2;
         assert(mid < m_end);
 
         PPP_DEBUG_MSG("Split: [" + to_string(m_start) + ", " + to_string(mid) + "] [" +
              to_string(mid) + ", " + to_string(m_end) + "]");
         ppp::TaskGroup tg;
         ReduceTask t1( m_array,m_start, mid, m_grainsize);
         ReduceTask t2(m_array, mid, m_end, m_grainsize);
         tg.spawn(t1);
        tg.spawn(t2);
         tg.wait();
       }
     private:
       T* m_array;
       int64_t m_start;
       int64_t m_end;
       int64_t m_grainsize;
     };
   }
  
  template <typename T>
  extern inline
  T parallel_reduce(T* array, int64_t start, int64_t end, int64_t grainsize=0)
  {    if (grainsize == 0) {
       grainsize = (end-start+1) / (get_thread_count()*4);
     }
     PPP_DEBUG_MSG("parallel_reduce grainsize: " + to_string(grainsize));
 
     internal::ReduceTask<T> t(array,start, end, grainsize);
     t.execute();
     PPP_DEBUG_MSG("parallel_reduce done");
    return internal::sum(array,start,end);
  }
}

#endif
