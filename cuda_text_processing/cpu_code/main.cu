#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <unordered_map>
#include <helper_timer.h>

#define HASHSIZE 500

#include "header.h"

using namespace std;

int num;
unsigned char* hash_map;
unsigned char* load_string(string s)
{
  ifstream file (s,ios::in | ios::binary);
  if(!file.is_open())
  {
	  printf("cannot open file");
	  return NULL;
  }
  file.seekg(0, ios_base::end);
  // Seek to the end to find length and seek back to begin
   int length = file.tellg();
  file.seekg(0, ios_base::beg);   
  int size = length + sizeof(char);
    char*  line = (char*)malloc(size);

  file.read(line,size);
  line[size-1] = '\0';
  return (unsigned char*) line;
}

void load_dict (int d_length)
{
	//int num_threads = num_blocks* threads_per_block;
	unsigned char *line = (unsigned char*)malloc(d_length*sizeof(unsigned char));
	line = load_string("dict.txt");
	
	StopWatchInterface *filterthetext = NULL;
	sdkCreateTimer(&filterthetext);
	sdkStartTimer(&filterthetext);
	filter_text(line, d_length);             
	sdkStopTimer(&filterthetext);
	float retval = sdkGetTimerValue(&filterthetext);
	printf("Dictionary Filtering Time: %f\n",retval);

	//hash_map
	 hash_map = NULL;
	hash_map  = (unsigned char*)malloc(HASHSIZE* 16000* sizeof(unsigned char));
	memset(hash_map, '-', HASHSIZE* 16000);

	//bin_demarcation
	unsigned int* bin_demarcation = NULL;
	unsigned int bin_demarcation_cpu[HASHSIZE] = {0};

	for(int counter = 0; counter < HASHSIZE; counter++)
	{
		bin_demarcation_cpu[counter] = 16000*counter;
	}
	StopWatchInterface *creatdict = NULL;
	sdkCreateTimer(&creatdict);
	sdkStartTimer(&creatdict);
	create_dict_on_cpu(line,d_length,bin_demarcation_cpu,hash_map);
	sdkStopTimer(&creatdict);
	float retval1 = sdkGetTimerValue(&creatdict);
	printf("Dictionary Creation Time: %f\n",retval1);
	printf("Total Time for Dictionary %f\n" , (retval +retval1));
}

void create_dict_on_cpu(unsigned char* textIn, int textLength, unsigned int* demarcation_array, unsigned char* hash_map )
{
	unsigned int i = 0;
	unsigned int start = 0;
	unsigned int end = 0;
	unsigned int iterator = 0;
	unsigned int hashValue = 0;
	
	for(i = 0; ;)
	{
		if (i == textLength-1) { break; }
        while (i < textLength && textIn[++i] == '-');
        if (i >= textLength) { break; }
        start = i;
        while (i < textLength && textIn[++i] != '-'); //find first not of '-' character
		end = i;

		hashValue = 0;
		//start and end are known., calculate hash value.
		for( iterator = start;iterator <end; iterator ++)
			hashValue = textIn[iterator] + 31*hashValue;
		hashValue = hashValue % HASHSIZE;

		int entry_start = demarcation_array[hashValue];
		for( iterator = start;iterator <end; iterator ++)
		{
			hash_map[entry_start] = textIn[iterator];
			entry_start++;
		}
		demarcation_array[hashValue] = demarcation_array[hashValue]+16;
	}
}


bool search_word_on_cpu(unsigned char* hash_map, unsigned char* textIn,int start, int end)
{
	unsigned int wordLength = end-start;
	unsigned int iterator = 0;
	unsigned int hashValue = 0;
	for( iterator = start;iterator <end; iterator ++)
		hashValue = textIn[iterator] + 31*hashValue;
	hashValue = hashValue % HASHSIZE;

	unsigned int serachLimitStart = 16000*hashValue;
	unsigned int serachLimitEnd = 16000*(hashValue+1);

	bool found = false;
	unsigned int searchDemarcation =0;
	unsigned int searchStart = 0;
	for(searchDemarcation = serachLimitStart; searchDemarcation < serachLimitEnd; searchDemarcation= searchDemarcation+16)
	{
		searchStart = searchDemarcation;
		for(iterator = start; iterator <end; iterator++)
		{
			if(textIn[iterator] != hash_map[searchStart])
				break;
			searchStart++;
		}
		if( hash_map[searchStart] == '-' && wordLength == (iterator - start))
		{
			found = true;
			break;
		}
	}
	return found;
}

void load_input_text(int input_length, unsigned char* hash_map)
{
	//int num_threads = num_blocks* threads_per_block;
	unsigned char *input_text = (unsigned char*)malloc(input_length*sizeof(unsigned char));
	input_text = load_string("input.txt");
	
	StopWatchInterface *filtinput = NULL;
	sdkCreateTimer(&filtinput);
	sdkStartTimer(&filtinput);
	filter_text(input_text, input_length);
	sdkStopTimer(&filtinput);
	float retval2 = sdkGetTimerValue(&filtinput);
	printf("Input Filtering Time: %f\n",retval2);

	//unsigned int* demarcation_array = (unsigned int*)malloc(num_threads*sizeof(unsigned int));

	//cudaMalloc thread_ids
	//unsigned int* thread_ids = (unsigned int*)malloc(num_threads*sizeof(unsigned int));

	//host for Ids
	//unsigned int* host_ids = (unsigned int*)malloc(num_threads*sizeof(unsigned int));

	/*
	int chunkLength = input_length/num_threads;
	process_input_text (input_text, input_length, demarcation_array, thread_ids, chunkLength, num_threads);
	demarcation_array[0] = 0;
 	demarcation_array[num_threads] = input_length-1;	// set the last */
	
	//code to extract the word and search on the dictionary hash table
	//unsigned char* word = (unsigned char*)malloc(16*sizeof(char));
	 num = 0 ;
	 StopWatchInterface *search = NULL;
				sdkCreateTimer(&search);
				sdkStartTimer(&search);
	for(int i=0; i<input_length;i++)
	{
			for (i  = 0;;)
			{
			  if (i == input_length-1) { break; }
			
			 while (i  < input_length && input_text[++i] == '-'); //find first '-' character
 
			 if (i >= input_length) { break; }
 
					// wordStart = textIn[count];
			 int begin = i;
			 while (i < input_length && input_text[++i] != '-'); //find first not of '-' character 
		  {
			    
			bool res = search_word_on_cpu(hash_map,input_text,begin,i);
			if(res!= 1)
			{
				num++;
			}
			}
		}
	}
	sdkStopTimer(&search);
			float retval3 = sdkGetTimerValue(&search);
			printf("Dictionary Search Time: %f\n",retval3);
			printf("Total time to correct : %f\n",(retval2+retval3));
}


int main()
{
	int length;
	//find the length of the file
	ifstream file ("dict.txt",ios::in | ios::binary);
  if(!file.is_open())
  {
	  printf("cannot open file");
	  return NULL;
  }
  file.seekg(0, ios_base::end);
   length = file.tellg();

	//load the file containing the dictionary text
	load_dict(length);


	//load the input file
	ifstream input_file ("input.txt",ios::in | ios::binary);
  if(!input_file.is_open())
  {
	  printf("cannot open file");
	  return NULL;
  }
  input_file.seekg(0, ios_base::end);
   length = input_file.tellg();
   load_input_text(length, hash_map);
   printf("errors: %d \n" , num);
}