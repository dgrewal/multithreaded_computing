#pragma once

#ifndef _HEADER_H_
#define _HEADER_H_
 
void filter_text(unsigned char *text_dict, int size);
void create_dictionary(unsigned char* textIn, int textLength, int* countOut, unsigned char* word, unsigned int* demarcation_array);



void create_dict_on_cpu(unsigned char* textIn, int textLength, unsigned int* demarcation_array, unsigned char* hash_map );
void createDictionary(char* textIn, int length);
void call();
#endif