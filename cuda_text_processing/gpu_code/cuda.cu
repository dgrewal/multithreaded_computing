#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
#include "header.h"
#include "device_functions.h"

#define HASHSIZE 1000

__device__ char lower(unsigned char letter)
{
    if (letter >= 65 && letter <= 90)
		{
		letter += 32;		// adding 32 to ansi converts it into lowercase
		return letter;
		}
	else
	{ if( letter >= 97)	// if already lowercase then no changes
		{ return letter;}
	else
	{return '-';}
}
}
__global__ void filter_text(unsigned char *text_dict,unsigned long long int size, int num_threads,unsigned long long int chunkLength)  //job_size is simply size/num_threads , k=1, j=0.. just so threads dont change it 
{
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if(i<size)
	{
		//transform to lowercase and add -
		for(int j = i*chunkLength; j<((i+1)*chunkLength);j++)
		{
			text_dict[j] = lower(text_dict[j]);	
		}	
	}
}

__device__ bool search_word_on_cpu(unsigned char* hash_map, unsigned char* textIn,int start, int end)
{
	unsigned int wordLength = end-start;
	unsigned int iterator = 0;
	unsigned int hashValue = 0;
	for( iterator = start;iterator <end; iterator ++)
		hashValue = textIn[iterator] + 31*hashValue;
	hashValue = hashValue % HASHSIZE;

	unsigned int serachLimitStart = 16000*hashValue;
	unsigned int serachLimitEnd = 16000*(hashValue+1);

	bool found = false;
	unsigned int searchDemarcation =0;
	unsigned int searchStart = 0;
	for(searchDemarcation = serachLimitStart; searchDemarcation < serachLimitEnd; searchDemarcation= searchDemarcation+16)
	{
		searchStart = searchDemarcation;
		for(iterator = start; iterator <end; iterator++)
		{
			if(textIn[iterator] != hash_map[searchStart])
				break;
			searchStart++;
		}
		if( hash_map[searchStart] == '-' && wordLength == (iterator - start))
		{
			found = true;
			break;
		}
	}
	return found;
}


__global__ void process_input_text_dict(unsigned char* textIn, int textLength, int* countOut, unsigned int* demarcation_array, unsigned int* thread_ids, int chunkLength, int numOfThreads)
{
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	thread_ids[i] = i;

	int offset = (chunkLength*i)-1;
	if(i != 0)
	{
		while(textIn[offset] != '-')
		{
			offset++;
		}
	}
	
	demarcation_array[i] = offset;
	
}

__global__ void process_input_text(unsigned char* textIn, unsigned int* demarcation_array, unsigned int* thread_ids, unsigned long long int chunkLength, int num_threads,unsigned long long int input_length)
{
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	thread_ids[i] = i;

	int offset = (chunkLength*i)-1;
	if(i != 0)
	{
		while(textIn[offset] != '-')
		{
			offset++;
		}
	}
	
	demarcation_array[i] = offset;
	demarcation_array[num_threads] =input_length;
	demarcation_array[0] = 0;
	
}

__global__ void check(unsigned char* textIn,unsigned long long int textLength,unsigned int* countOut, unsigned int* demarcation_array, unsigned char* hash_map)
{
	int i = threadIdx.x+ blockIdx.x*blockDim.x;
//	unsigned long long int num =0;
	bool res = false;
	for( unsigned long long int count = demarcation_array[i]; count<demarcation_array[i+1];/*count++*/)
	{
		  if (count == demarcation_array[i+1]-1) { break; }
		
   		  while (count  < demarcation_array[i+1] && textIn[++count] == '-'); //find first '-' character
 
          if (count >= demarcation_array[i+1]) { break; }
 
	      unsigned long long int begin = count;
          while (count < demarcation_array[i+1] && textIn[++count] != '-'); //find first not of '-' character 
		  {
			res = false;
			res = search_word_on_cpu(hash_map,textIn,begin,count);
			if(res!= true)
			{	
				atomicAdd(countOut, 1);
				//num++;
			}
		  }
	}
//	*countOut = num;
}