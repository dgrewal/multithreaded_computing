#pragma once

#ifndef _HEADER_H_
#define _HEADER_H_
 
__global__ void filter_text(unsigned char *text_dict, unsigned long long int size, int num_threads,unsigned long long int chunkLength);
__global__ void create_dictionary(unsigned char* textIn, int textLength, int* countOut, unsigned char* word, unsigned int* demarcation_array);
__global__ void process_input_text(unsigned char* textIn, unsigned int* demarcation_array, unsigned int* thread_ids, unsigned long long int chunkLength,int num_threads,unsigned long long int input_length);
__global__ void process_input_text_dict(unsigned char* textIn, int textLength, int* countOut, unsigned int* demarcation_array, unsigned int* thread_ids, int chunkLength, int numOfThreads);
__global__ void check(unsigned char* textIn,unsigned long long int textLength,unsigned int* countOut, unsigned int* demarcation_array, unsigned char* hash_map);



void create_dict_on_cpu(unsigned char* textIn,unsigned long long int textLength, unsigned int* demarcation_array, unsigned char* hash_map );
void createDictionary(char* textIn, int length);
void call();
#endif