#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>
#include <algorithm>
#include <set>
#include <map>
#include <unordered_map>
#include <helper_timer.h>

#define HASHSIZE 1000
//#define SEARCH_THREADS 2

#include "header.h"

using namespace std;
#define num_blocks 4
#define threads_per_block 384

unsigned long long int length;
unsigned char* hash_map;
unsigned char* load_string(string s)
{
  ifstream file (s,ios::in | ios::binary);
  if(!file.is_open())
  {
	  printf("cannot open file");
	  return NULL;
  }
  file.seekg(0, ios_base::end);
  // Seek to the end to find length and seek back to begin
   length = file.tellg();
  file.seekg(0, ios_base::beg);   
  unsigned long long int size = length + sizeof(char);
    char*  line = (char*)malloc(size);

  file.read(line,size);
  line[size-1] = '\0';
  return (unsigned char*) line;
}

void load_dict (unsigned long long int d_length)
{
	int num_threads = num_blocks* threads_per_block;
	unsigned char *line = (unsigned char*)malloc(d_length*sizeof(unsigned char));
	line = load_string("dict.txt");
	unsigned char * d_line;

	//malloc for input text
	cudaError_t err = cudaMalloc((void**)&d_line, d_length*sizeof(unsigned char));
	if(err!= cudaSuccess){ printf("error Malloc  %s\n", cudaGetErrorString(err));}

	//Memcpy Input text
	cudaError_t err1 = cudaMemcpy(d_line, line, d_length, cudaMemcpyHostToDevice);
	if(err1!= cudaSuccess){ printf("error Memcpy to Device  %s\n",cudaGetErrorString(err1));}

	//timer for dict filtering
	StopWatchInterface *filter_dict = NULL;
	sdkCreateTimer(&filter_dict);
	sdkStartTimer(&filter_dict);
	filter_text<<<num_blocks,threads_per_block>>>(d_line, d_length,num_threads,(unsigned long long int)((d_length/num_threads)+1));
	sdkStopTimer(&filter_dict);
	float retval = sdkGetTimerValue(&filter_dict);
	printf("Dictionary Filtering Time: %f\n",retval);

	//memcpy the input text back
	cudaError_t err2 = cudaMemcpy(line,d_line,d_length,cudaMemcpyDeviceToHost);
	if(err2!= cudaSuccess){ printf("error Memcpy from device  %s\n",cudaGetErrorString(err));}	

	//hash_map
	 hash_map = NULL;
	hash_map  = (unsigned char*)malloc(HASHSIZE* 16000* sizeof(unsigned char));
	memset(hash_map, '-', HASHSIZE* 16000);

	unsigned int bin_demarcation_cpu[HASHSIZE] = {0};
	for(int counter = 0; counter < HASHSIZE; counter++)
	{
		bin_demarcation_cpu[counter] = 16000*counter;
	}

	StopWatchInterface *create_dict = NULL;
	sdkCreateTimer(&create_dict);
	sdkStartTimer(&create_dict);
	create_dict_on_cpu(line,d_length,bin_demarcation_cpu,hash_map);
	sdkStopTimer(&create_dict);
	 float retval1 = sdkGetTimerValue(&create_dict);
	printf("Dictionary Creation Time: %f\n",retval1);

	float total = retval + retval1;
	printf("Total Time for Dictionary :%f \n \n \n", total);
	cudaFree(d_line);
}

void create_dict_on_cpu(unsigned char* textIn,unsigned long long int textLength, unsigned int* demarcation_array, unsigned char* hash_map )
{
	unsigned int i = 0;
	unsigned int start = 0;
	unsigned int end = 0;
	unsigned int iterator = 0;
	unsigned int hashValue = 0;
	for(i = 0; ;)
	{
		if (i == textLength-1) { break; }
        while (i < textLength && textIn[++i] == '-');
        if (i >= textLength) { break; }
        start = i;
        while (i < textLength && textIn[++i] != '-'); //find first not of '-' character
		end = i;

		hashValue = 0;
		//start and end are known., calculate hash value.
		for( iterator = start;iterator <end; iterator ++)
			hashValue = textIn[iterator] + 31*hashValue;
		hashValue = hashValue % HASHSIZE;

		int entry_start = demarcation_array[hashValue];
		for( iterator = start;iterator <end; iterator ++)
		{
			hash_map[entry_start] = textIn[iterator];
			entry_start++;
		}
		demarcation_array[hashValue] = demarcation_array[hashValue]+16;
	}
}



void load_input_text(unsigned long long int input_length, unsigned char* hash_map)
{
	int num_threads = num_blocks* threads_per_block;
	unsigned char *input_text = (unsigned char*)malloc(input_length*sizeof(unsigned char));
	input_text = load_string("input.txt");
	unsigned char * d_input_text;

	//malloc for input text
	cudaError_t err = cudaMalloc((void**)&d_input_text, input_length*sizeof(unsigned char));
	if(err!= cudaSuccess){ printf("error Malloc  %s\n", cudaGetErrorString(err));}

	//Memcpy Input text
	cudaError_t err1 = cudaMemcpy(d_input_text, input_text, input_length, cudaMemcpyHostToDevice);
	if(err1!= cudaSuccess){ printf("error Memcpy to Device  %s\n",cudaGetErrorString(err1));}
	

//	StopWatchInterface *filter_input = NULL;
//	sdkCreateTimer(&filter_input);
//	sdkStartTimer(&filter_input);
	filter_text<<<num_blocks,threads_per_block>>>(d_input_text, input_length,num_threads,(unsigned long long int)((input_length/num_threads)+1));
	//cudaDeviceSynchronize();
//	sdkStopTimer(&filter_input);
//	float retval = sdkGetTimerValue(&filter_input);
//	printf("Input Filtering Time: %f\n",retval);

		//calculate the demarcation array
	//cudaMalloc Demarcation Array
	unsigned int* demarcation_array = NULL;
	cudaError_t error = cudaMalloc((void**)&demarcation_array, (num_threads+1)*sizeof(unsigned int));
	if(cudaSuccess != error)
		printf("error");

	//cudaMalloc thread_ids
	unsigned int* thread_ids = NULL;
	error = cudaMalloc((void**)&thread_ids, num_threads*sizeof(unsigned int));
	if(cudaSuccess != error)
		printf("error");

	//cudaMalloc device_counter
	unsigned int* device_counter = NULL;
	error = cudaMalloc((void**)&device_counter , sizeof(unsigned int));
	if(cudaSuccess != error)
		printf("error");
	cudaMemset(device_counter, 0, sizeof(unsigned int));

	//hashmap copy to GPU
	unsigned char* d_hash_map;
	cudaError_t err_mal_hmap = cudaMalloc((void**)&d_hash_map, HASHSIZE* 16000* sizeof(unsigned char));
	if(err_mal_hmap != cudaSuccess){printf("error mal hmap %s \n",cudaGetErrorString(err_mal_hmap));}
	cudaError_t err_mcpy_hmap = cudaMemcpy(d_hash_map,hash_map,HASHSIZE* 16000* sizeof(unsigned char), cudaMemcpyHostToDevice);
	

	unsigned long long int chunkLength = length/num_threads;
	//call the process_input_text to find offsets
	StopWatchInterface *process_input = NULL;
	sdkCreateTimer(&process_input);
	sdkStartTimer(&process_input);
	process_input_text<<<num_blocks,threads_per_block>>> (d_input_text, demarcation_array, thread_ids, chunkLength,num_threads,length);
	check<<<num_blocks,threads_per_block>>>(d_input_text,length,device_counter,demarcation_array,d_hash_map);
	cudaDeviceSynchronize();
	sdkStopTimer(&process_input);
	float retval1 = sdkGetTimerValue(&process_input);
    	printf("total Time: %f\n",retval1);
	

	
	unsigned int* host_counter = (unsigned int*)malloc(sizeof(unsigned int));
	// copy stuff back to CPU - Sanity checks...
	cudaError_t erry = cudaMemcpy(host_counter,device_counter,sizeof(unsigned int),cudaMemcpyDeviceToHost);
	if(erry!=cudaSuccess){printf("error memcpy counter bak to host\n");}

	printf("errors : %u \n",*host_counter);

	cudaFree(d_input_text);
	cudaFree(device_counter);
	cudaFree(d_hash_map);
	cudaFree(demarcation_array);
}




int main()
{
	//find the length of the file
	ifstream file ("dict.txt",ios::in | ios::binary);
  if(!file.is_open())
  {
	  printf("cannot open file");
	  return NULL;
  }
  file.seekg(0, ios_base::end);
   length = file.tellg();

	//load the file containing the dictionary text
	load_dict(length);


	//load the input file
	ifstream input_file ("input.txt",ios::in | ios::binary);
  if(!input_file.is_open())
  {
	  printf("cannot open file");
	  return NULL;
  }
  input_file.seekg(0, ios_base::end);
   length = input_file.tellg();

	//load the file containing the dictionary text
	load_input_text(length, hash_map);
	
}